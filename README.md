---
author: Cedric Chariere Fiedler
---
# One Pager

With **One Pager**, transforming your data into a nice and beautiful 
html file has never be so easy!

Because a **Readme.md** file stays stern and colorless, transform it into
a beautiful landing-page with a single input into your CI process!

You want to agregate all your generated content (tests reports and coverages,
 documentation, technical and functional design documents...) in a nice portal?
Let's try our amazing tool!

Do you have funnier use cases to sublimate your data? 

To infinity and beyond with **One Pager**!

## Using as binary

**One Pager** can be used as binary. We are working on a server integration.

### Install Globally

**One-Pager** is a ***npm*** package and can be installed by typing the following line:

```sh
npm install -g @puzle/one-pager
one-pager .
```

### (Alternative) Install locally

You can install our tool as a local dependency and use it with ***npx***

```
npm install @puzle/one-pager
npx one-pager .
```

### Display the help

You can display the help and list each options by typing:

```sh
one-page -h
```

## The Configuration file

By default, the **One-Pager** binary searchs on the current directory a configuration file named `one-pager.json`.


You can override the path with the option `-c`

```sh
one-pager -c ./config/one-pager.portal.json
```
 
### The configuration file format

The configuration file must follow this format:

```json 
{
    "template": "http://path-to-my-template.com/index.html",
    "destination": "./dist/result.html",
    "data": [
        {
            "url": "readme.md"
        }, {
            "url": "./metadata.yml"
        }, {
            "url": "./package.json"
        }
    ]
}
```

**template**:  
The location of the template used during the compilation step. See the relative section to know how to write your own template.

**destination**:  
The destination of the generated file. You have to give the full name, including the filename.

**data**:  
Several data can be used to hydrate the template file. Their order is important because it defines the data priority. If two files have the same keys, only the ones placed at the lesser position will be keeped.
You can integrate local and remote files. The remote files must be accessible by the http or https protocols.

## Our Roadmap

- Testing
  - 100% code coverage for the Unit Testing
  - e2e testing
- Documentation
  - Auto Generation
  - Deploy the doc as Gitlab Pages content
- Add a HTML pages based on One Pager
- Features
  - ```one-pager init``` to generate an empty config file with the default fields
  - Support of a static folder to copy into the final destination
  - Rework the destination option: use only a folder and specify the final name by a specific configuration option

## License

BSD
