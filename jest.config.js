const { pathsToModuleNameMapper } = require('ts-jest/utils');
const { compilerOptions } = require('./tsconfig');

module.exports = {
    moduleFileExtensions: [
      "ts",
      "tsx",
      "js",
      "json"
    ],
    transform: {
      "^.+\\.tsx?$": "ts-jest"
    },
    moduleDirectories: [
      "src", "node_modules"
    ],
    testRegex: "/src.*\\.(test|spec).(ts|tsx|js)$",
    collectCoverageFrom : ["src/**/*.{js,jsx,tsx,ts}", "!**/node_modules/**", "!**/vendor/**"],
    coverageReporters: ["json", "lcov"],
    moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths , { prefix: '<rootDir>/' } )
};
