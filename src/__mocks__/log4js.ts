
export function getLogger(_: string) {
  return {
    danger: ( __: any) => __,
        // tslint:disable-next-line: no-console
    error: console.error,
    info: ( __: any) => __,
  };
}
