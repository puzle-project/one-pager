import * as fs from 'fs';
import * as path from 'path';
import { promisify } from 'util';

import { getLogger } from 'log4js';

import * as hbs from 'handlebars';
import * as marked from 'marked';

import { loadOneFile } from '@loader/loader';
import { loadAllDataFiles } from '@metadata/load_metadata';
import { IConfigFileOptions } from './IConfigFileOptions';

const logger = getLogger('compile');

const defaultConfigPath = 'one_pager.json';

/**
 * 
 * @param template 
 * @param data 
 * @param destinationPath 
 */
export async function compile(
    template: string,
    data: any,
    destinationPath: string,
): Promise<any> {

    const mergedContent =
        data.filter( (item: any) => 'content' in item)
            .map((item: {content: string}) => item.content)
            .join('\n');

    // Markdown Compilation
    const content = marked(mergedContent);

    const mergedData =
        Object.assign({}, ...
        data.filter( (item: any) => 'data' in item)
            .map((item: {data: any}) => item.data ),
        );

    const toBind = Object.assign({}, mergedData, {
        content,
    });

    const compiledTemplate = hbs.compile(template);
    const result = compiledTemplate(toBind);

    // Write final result
    return await promisify(fs.writeFile)(destinationPath, result, 'utf8');
}

function getConfigPath(
    configPath: string,
): string {
    return path.join(process.cwd(), configPath ? configPath : defaultConfigPath);
}

export function getFileConfig(
    configPath: string,
): any {

    let config: IConfigFileOptions;
    const finalConfigPath = getConfigPath(configPath);

    logger.info(finalConfigPath);

    try {
        config = require(finalConfigPath);
    } catch (err) {
        throw new Error(`Cannot load the Config File at: ${configPath}`);
    }

    return config;
}

/**
 * Return a cleaned object, whose undefined or empty fields have been removed
 * @param item<object> the object to clean
 * @return <object> The cleaned object
 */
function cleanObject(item: object): object {
    const newItem = {};

    Object
        .keys(item)
        .forEach((key: string) => {
            if (item[key] !== undefined && item[key] !== '') {
                newItem[key] = item[key];
            }
        });

    return newItem;
}
/**
 * Merge together all the Config Files. Each empty key (undefined of empty) is removed.
 * The order of the contained object defines the priority of keys:
 * if two items have the same key, only the last one will be keeped.
 * By example:
 * ```js
 * const foo = {a: 1, b: 2, c:''};
 * const bar = {a:20, c: undefined};
 * const result = getFinalConfig(foo, bar);
 * ```
 * The expected value of result is:
 * ```js
 * result === {a: 20, b:2};
 * ```
 * @param configs <IConfigFileOptions[]> The config files to merge
 * @return a final config file, cleaned and merged by priority
 */
export function getFinalConfig(
    ... configs: IConfigFileOptions[]
): IConfigFileOptions {

    const cleanedConfig = configs.map(cleanObject);

    return Object.assign({}, ...cleanedConfig);
}

/**
 * From a given config file option, the function get the data from several files,
 * downloads the template to use, compiles them into a final html file and writes it
 * in the specific destination.
 * The build function is the main process of this tool.
 */
export async function build(
    config: IConfigFileOptions,
) {

    try {
    // Get content
        try {

            const template = await loadOneFile(config.template, config.currentDirectory);
            const metadata = await loadAllDataFiles(config.data, config.currentDirectory);

            logger.info(metadata);

            await compile(
                template,
                metadata,
                path.resolve(config.currentDirectory, config.destination),
            );

        } catch (err) {
            logger.error(err);
        }
    } catch (err) {
        logger.error(err);
    }
}
