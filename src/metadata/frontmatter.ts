import * as yaml from 'js-yaml';

/**
 * Get the frontmatter data as string from the given raw markdown content,
 * otherwise, the function return an empty string
 * @param markdownContent <string> The raw markdown content to parse
 * @param separator <string> The separator, '---' by default
 * @return the frontmatter data as string, otherwise an empty string
 */
export function extractFrontmatter(markdownContent: string, separator?: string): string {
    separator = separator || '---';

    const tiles = markdownContent.split(separator);
    return tiles.length > 2 ? tiles[1] : '';
}

/**
 * Get the frontmatter data as Object and the content as string
 * (an empty string by default)
 * @param markdownContent <string> The raw markdown content to parse
 * @param separator <string> The separator, '---' by default
 * @return {data: Object, content: string} The extracted data are
 * represented as JS object (from yaml) and the content as string
 */
export function splitContentFromData(markdownContent: string, separator?: string): {data: object; content: string} {
    const result =  {
        content: '',
        data: {},
    };

    separator = separator || '---';
    const tiles = markdownContent.split(separator);

    if (tiles.length > 2) {
        result.data = yaml.load(tiles[1]);
        result.content = tiles[2];
    } else {
        result.content = markdownContent;
    }

    return result;
}
