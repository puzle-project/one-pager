import {extractFrontmatter, splitContentFromData} from './frontmatter';

describe('extract_frontmatter', () => {
  it('should extract with the default separator', () => {
    const rawData = `---
author: "John Doe"
website: "http://john.doe"
---
content`;

    const expectedResult = `
author: "John Doe"
website: "http://john.doe"
`;

    const result = extractFrontmatter(rawData);
    expect(result).toEqual(expectedResult);
  });

  it('should extract with a custom seperator', () => {
    const rawData = `~~~~~
author: "John Doe"
website: "http://john.doe"
~~~~~
content`;

    const expectedResult = `
author: "John Doe"
website: "http://john.doe"
`;
    const customSeparator = '~~~~~';
    const result = extractFrontmatter(rawData, customSeparator);
    expect(result).toEqual(expectedResult);
  });

  it('should return an empty string if the data has any separator', () => {
    const rawData = 'lorem ipsum';

    const result = extractFrontmatter(rawData);
    expect(result).toEqual('');
  });
});

describe('splitContentFromData', () => {

    it('should split the content if a frontmatter section is existing', () => {
        const rawData = `
---
author: "lorem ipsum"
---
dolore amet
`;
        const result: {data: object; content: string} = splitContentFromData(rawData);
        expect(result.data).toStrictEqual({author: 'lorem ipsum'});
        expect(result.content).toBe('\ndolore amet\n');
    });

    it('should return an empty data object if the frontmatter is not existing', () => {
        const rawData = `
dolore amet
`;
        const result: {data: object; content: string} = splitContentFromData(rawData);
        expect(result.data).toStrictEqual({});
        expect(result.content).toBe('\ndolore amet\n');
    });

    it('should return an empty data object and empty string if the markdown content is empty', () => {
        const rawData = '';
        const result: {data: object; content: string} = splitContentFromData(rawData);
        expect(result.data).toStrictEqual({});
        expect(result.content).toBe('');
    });
});
