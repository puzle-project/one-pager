import { IMetadataFile } from 'src/IConfigFileOptions';
import {loadAllDataFiles} from './load_metadata';

jest.mock('@loader/loader');

// Remark: the use of TS import does not import the mocked
// service, that's why we are requiring in a node way
// tslint:disable-next-line: no-var-requires
const loader = require('@loader/loader');

describe('load', () => {

  it('should get data from a JSON file', async () => {

    loader.__setMockedContent__({
      'file.json': `{
        "content": "i m content",
        "arithmetic": 4
      }`,
    });

    const finalMetadataFile: IMetadataFile[] = [{
      options: {},
      url: 'file.json',
    }];

    return expect(loadAllDataFiles(finalMetadataFile, '')).resolves.toStrictEqual([{
      content: 'i m content',
      data: {
        arithmetic: 4,
      },
    }]);

  });

  it('should get data from a markdown file', async () => {

    loader.__setMockedContent__({
      'file.md': `
---
arithmetic: 4
---
i m content
`,
    });

    const finalMetadataFile: IMetadataFile[] = [{
      options: {},
      url: 'file.md',
    }];

    return expect(loadAllDataFiles(finalMetadataFile, '')).resolves.toStrictEqual([{
      content: '\ni m content\n',
      data: {
        arithmetic: 4,
      },
    }]);
  });

  it('should get data from a yaml file', async () => {

    loader.__setMockedContent__({
      'file.yml': `
arithmetic: 4
content: "i m content"`,
    });

    const finalMetadataFile: IMetadataFile[] = [{
      options: {},
      url: 'file.yml',
    }];

    return expect(loadAllDataFiles(finalMetadataFile, '')).resolves.toStrictEqual([{
      content: 'i m content',
      data: {
        arithmetic: 4,
      },
    }]);
  });

});
