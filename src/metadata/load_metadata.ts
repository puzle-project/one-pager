import * as yaml from 'js-yaml';
import * as path from 'path';

import {getLogger} from 'log4js';

import {loadOneFile} from '@loader/loader';
import {extractFrontmatter, splitContentFromData} from './frontmatter';

import { IMetadataFile, IMetadataOptions } from 'src/IConfigFileOptions';

const logger = getLogger('load_metadata');

function fromYamlFormatToJs(
    rawdata: string,
    options: IMetadataOptions,
) {

    const {content, ...data} = yaml.load(rawdata);
    return {data, content};
}

function fromJsonFormatToJs(
    rawdata: string,
    options: IMetadataOptions,
) {
    const {content, ...data} = JSON.parse(rawdata);

    return {data, content};
}

function fromMarkdownFormatToJs(
    data: string,
    options: IMetadataOptions,
) {
    return splitContentFromData(data);
}

function formatDataToJs(
    data: string,
    options: IMetadataOptions,
): any {

    let result = {};

    if (!options.data_type) {
        throw new Error('The data_type must be given to load metadata!');
    }

    switch (options.data_type) {
        case '.json':
            result = fromJsonFormatToJs(data, options);
            break;
        case '.yml':
            result = fromYamlFormatToJs(data, options);
            break;
        case '.md':
            result = fromMarkdownFormatToJs(data, options);
            break;
        default:
            logger.error('Cannot load the type:', options.data_type);
    }

    return result;
}

async function loadOneDataFile(
    url: string,
    configDirectory: string,
): Promise<any> {

    const dataType = path.extname(url);
    const finalOptions = {data_type: dataType};
    const data = await loadOneFile(url, configDirectory);
    logger.info(`load_one ${url} - data`, data,  finalOptions, '-');

    return Promise.resolve(formatDataToJs(data, finalOptions));
}

export async function loadAllDataFiles(
    metadataFiles: IMetadataFile[],
    configDirectory: string,
): Promise<any> {

    const all = [];
    logger.info(`metadataFiles: ${metadataFiles}`);

    for (const file of metadataFiles) {
        logger.info(`Load metadata file ${file.url}`);
        const metadata = await loadOneDataFile(file.url,  configDirectory);
        all.push(metadata);
    }

    return all;
}
