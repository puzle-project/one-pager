export interface IMetadataOptions {
    safeload?: boolean;
    data_type?: string;
}

export interface IMetadataFile {
    url: string;
    options: IMetadataOptions;
}

export interface IConfigFileOptions {
    template?: string;
    destination?: string;
    data?: IMetadataFile[];
    currentDirectory?: string;
}
