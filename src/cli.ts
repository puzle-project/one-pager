#!/usr/bin/env node

import 'module-alias/register';

import * as program from 'commander';
import * as path from 'path';

import {configure, getLogger} from 'log4js';

import {build, getFileConfig, getFinalConfig} from './compile';
import * as log4jsConfig from './log4js.json';

import { IConfigFileOptions } from './IConfigFileOptions';

const logger = getLogger('cli');

configure(log4jsConfig);

program
  .version('0.4.0')
  .usage('[options]')
  .option('-c, --configfile [configPath]', 'The config file path')
  .option('--destination [destinationPath]', 'The destination file path')
  .option('-d --data [datafilesPaths]', 'The metadata files used to inject into the template')
  .option('-t --template [templatePath]', 'The template file path')
  .parse(process.argv);

const defaultConfig: IConfigFileOptions = {
  destination: 'dist/index.html',
  template: './template/default.html',
};

const cmdConfig: IConfigFileOptions = {
  currentDirectory:
    program.configPath ?
      path.dirname(program.configPath) :
      process.cwd(),
  data: program.datafilesPaths,
  destination: program.destinationPath,
  template: program.templatePath,
};

const configPath = program.configPath || 'one-pager.json';

try {
  const fileConfig: IConfigFileOptions = getFileConfig(configPath);
  const finalConfig = getFinalConfig(defaultConfig, fileConfig, cmdConfig);

  logger.info(finalConfig);
  build(finalConfig);

} catch (err) {
  logger.error(err);
}
