import { getLogger } from 'log4js';

const logger = getLogger('loader.spec');

let mockedContent = {};

export function __addMockedContent__(content: object) {
  mockedContent = Object.assign(mockedContent, content);
}

export function __setMockedContent__(content: object) {
  mockedContent = content;
}

export async function loadOneFile(url: string, configPath: string): Promise<string> {
  return new Promise((resolve, reject) => resolve(mockedContent[url] || ''));
}
