import * as fs from 'fs';
import * as path from 'path';
import {promisify} from 'util';

import {getLogger} from 'log4js';

const logger = getLogger('localLoader');

export async function loadFromLocal(
    unresolvedUrl: string,
    configPath: string,
    readMode: string = 'utf8',
): Promise<string> {
    logger.info(`unresolvedUrl ${unresolvedUrl} - configPath: ${configPath}`);
    const url = path.resolve(configPath, unresolvedUrl);
    logger.info(`loadFromLocal ${url}`);
    return await promisify(fs.readFile)(url, readMode);
}
