import { getLogger } from 'log4js';

import {loadFromLocal} from './localLoader';
import {loadFromWeb} from './webLoader';

const logger = getLogger('loader');

/**
 * isExternal return *true* if the given link is an url prefixed by *http(s?)*
 * @param url <string> the link to check
 * @return <boolean> The result of the regexp test
 */
function isExternal(url: string): boolean {
    return /^http(s?):\/\//.test(url);
}

/**
 * Async: loadFile load the content of a file (locally or remotely) and return it
 * @param url <string> the link to the file to load
 * @param configPath <string> In case of a relative link, the config path  defines
 * the root path to resolve with the url: *configPath/url*
 * @return <Promise<string>> The file content as Promise
 * @throws If and error occured during the file loading, return a global error
 */
export async function loadOneFile(url: string, configPath: string): Promise<string> {
    // return content
    let result;
    try {
        if (isExternal(url)) {
            logger.info('Load external file: ', url);
            result = await loadFromWeb(url);
        } else {
            logger.info('Load local file: ', url, configPath);
            result = await loadFromLocal(url, configPath);
        }
    } catch (err) {
        const errorMsg = `An error occured during the file loading: ${configPath} - ${url} \n ${err}`;
        throw new Error(errorMsg);
    }
    return result;
}
