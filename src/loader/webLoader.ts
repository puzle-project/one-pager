import * as http from 'http';
import * as https from 'https';

const loader = {
    http,
    https,
};

/**
 * Async: loadFromWeb loads the content of a remote file
 * Only the *http* and the *https* protocols are supported
 * If an error occured during the file loading, the promise is
 * rejected
 * @param url <string> The file link to load
 * @return <Promise<string>> The file content
 */
export async function loadFromWeb(urlAsString: string): Promise<string> {

    const url = new URL(urlAsString);

    return new Promise((resolve, reject) => {
        loader[url.protocol].get(url, (res) => {
            let data = '';
            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                resolve(data);
            });

        }).on('error', (err) => {
            reject(err);
        });
    });
}
